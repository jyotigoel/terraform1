#variables.tf file
variable "cidr_block_vpc" {
  default = ""
}
variable "tags" {
  type    = any
  default = {}
}


variable "tags_subnet1" {
  type    = any
  default = {}
}

variable "tags_subnet2" {
  type    = any
  default = {}
}

variable "tags_igw" {
  type    = any
  default = {}
}


variable "cidr_block_subnet1" {
  default = ""
}

variable "cidr_block_subnet2" {
  default = ""
}

variable "ec2_ami" {
  default = ""
}

variable "ec2_type" {
  default = ""
}

