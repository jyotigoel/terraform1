# route table file 
resource "aws_route_table" "example" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "public_route_table"
  }

}

resource "aws_route_table_association" "public-subnet-1-route-table-association" {
  subnet_id = aws_subnet.main1.id

  route_table_id = aws_route_table.example.id
}


resource "aws_route_table_association" "public-subnet-2-route-table-association" {

  subnet_id      = aws_subnet.main2.id
  route_table_id = aws_route_table.example.id
}
