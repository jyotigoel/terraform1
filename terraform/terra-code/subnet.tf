#subnets file
resource "aws_subnet" "main2" {
  vpc_id     = aws_vpc.main.id
  cidr_block = var.cidr_block_subnet2

  tags = var.tags_subnet1

}

resource "aws_subnet" "main1" {
  vpc_id     = aws_vpc.main.id
  cidr_block = var.cidr_block_subnet1

  tags = var.tags_subnet2
}
