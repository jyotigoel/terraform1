# terraform.tfvars file
cidr_block_vpc = "10.0.0.0/16"
tags = {
  name = "vpc-terraform"
}


cidr_block_subnet1 = "10.0.0.0/17"
tags_subnet1 = {
  name = "subnet1"
}

cidr_block_subnet2 = "10.0.128.0/17"
tags_subnet2 = {
  name = "subnet2"
}

tags_igw = {
  name = "igw"
}

ec2_ami  = "ami-0033b96ee48ba0f56"
ec2_type = "t2.micro"
