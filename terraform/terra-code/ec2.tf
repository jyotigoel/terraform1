resource "aws_instance" "instance_public" {
  associate_public_ip_address = true
  subnet_id                   = aws_subnet.main1.id
  ami                         = var.ec2_ami
  instance_type               = var.ec2_type
  key_name                    = "Key-pair"
  vpc_security_group_ids      = [aws_security_group.allow_tls.id]
  tags = {
    Name = "Nginx"
    tool = "nginx"
  }
  credit_specification {
    cpu_credits = "unlimited"
  }
}


resource "aws_instance" "instance_public2" {
  associate_public_ip_address = true
  subnet_id                   = aws_subnet.main2.id
  ami                         = var.ec2_ami
  instance_type               = var.ec2_type
  key_name                    = "Key-pair"
  vpc_security_group_ids      = [aws_security_group.allow_tls.id]
  tags = {
    Name = "Nginx2"
    tool = "nginx"
  }
  credit_specification {
    cpu_credits = "unlimited"
  }
}
