output "vpc_id" {
  value = aws_vpc.main.id
}

output "subnet_id_1" {
  value = aws_subnet.main1.id
}
output "subnet_id_2" {
  value = aws_subnet.main2.id
}

